const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");
const Product = require("../models/Product")
const User = require("../models/User")
const Order = require("../models/Order")



// register
router.post("/register", (req, res) => {
	User.findOne({email : req.body.email}, (err, result) => {
	if(req.body.firstName == "" || req.body.lastName == "" || req.body.mobileNum == "" || req.body.email == "" || req.body.password == ""){
			return res.send("Please fill all the requirements")
		}
	else if(req.body.firstName == ""){
			return res.send("Please enter your first name")
		}
	else if(req.body.lastName == ""){
			return res.send("Please enter your last name")
		}
	else if(req.body.mobileNum == ""){
			return res.send("Please enter your mobile number")
		}		
/*	else if(req.body.email == "" && req.body.password == ""){
			return res.send("Please enter a email and password")
		}*/
	else if(req.body.email == ""){
			return res.send("Please enter an email")
		}
	else if(req.body.password == ""){
			return res.send("Please enter a password")
		}
/*	else if(req.body.firstName == "" && req.body.lastNmae == "" && req.body.mobileNum == "" && req.body.email == "" && req.body.password == ""){
			return res.send("Please fill all the requirements")
		}*/
	else if(result != null && result.email == req.body.email){
			return res.send("The email that you entered is already used, please enter a new one.")
		}

	userController.registerAccount(req.body).then(resultFromController => res.send(resultFromController))
	})
})
// get details using bearer token
router.get("/details", auth.verify, (req, res) => {
	
	const accountData = auth.decode(req.headers.authorization)
	
	userController.getAccount({userId : accountData.id}).then(resultFromController => res.send(resultFromController));
});
// log-in account to get bearer token
router.post("/login", (req, res) => {

	if(req.body.email == "" && req.body.password == ""){
			return res.send("Please enter a email and password")
		}
	else if(req.body.email == ""){
			return res.send("Please enter an email")
		}
	else if(req.body.password == ""){
			return res.send("Please enter a password")
		}
	userController.loginAccount(req.body).then(resultFromController => res.send(resultFromController));
});

// edit isAdmin
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.setAsAdmin(data, req.params).then(resultFromController => res.send(resultFromController));	
});

/////////////////

router.post("/checkout" , auth.verify,(req, res) => {

	let data = {
		userId : req.body.userId,
		productId : req.body.productId,
		orderId : req.body.orderId
	}

	let dat = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.order(data, dat).then(resultFromController => res.send(resultFromController));
})


////////

router.get("/orders", (req, res) => {

	const data ={
	isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.getAllOrders(data).then(resultFromController => res.send(resultFromController));
});

/////

router.get("/myOrders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	userController.getDetails({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});


module.exports = router;