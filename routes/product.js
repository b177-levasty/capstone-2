const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");
const Product = require("../models/Product")

// Route for creating a product
router.post("/", auth.verify, (req, res) => {

	Product.findOne({name : req.body.name}, (err, result) => {
	if(req.body.name == "" && req.body.description == "" && req.body.price == "" ){
			return res.send("Please fill all the requirements")
		}
	else if(req.body.name == ""){
			return res.send("Please enter a name")
		}
	else if(req.body.description == ""){
			return res.send("Please enter a description")
		}
	else if(req.body.price == ""){
			return res.send("Please enter a price")
		}
	else if(result != null && result.name == req.body.name){
			return res.send("There can be only one of this product")
		}

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
	})
});

// Route for getting Active products
router.get('/', (req, res) => {
    productController.getActiveProducts().then(resultsFromFindActive => res.send(resultsFromFindActive))
});

// Route for getting a specific product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for updating a product
router.put("/:productId", auth.verify, (req, res) => {
	Product.findOne({name : req.body.name}, (err, result) => {
	if(req.body.name == "" && req.body.description == "" && req.body.price == "" ){
			return res.send("Please fill all the requirements if you want to update")
		}
	else if(req.body.name == ""){
			return res.send("Please enter a name of the product")
		}
	else if(req.body.description == ""){
			return res.send("Please enter a description of the product")
		}
	else if(req.body.price == ""){
			return res.send("Please enter a price of the product")
		}
/*	else if(result != null && result.name == req.body.name){
			return res.send("There can be only one of this product")
		}
*/
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.updateProduct(data, req.params, req.body).then(resultFromController => res.send(resultFromController));
	})
});

// Route for updateing isActive to true
/*router.put("/:productId/archive", auth.verify, (req, res) => {
	productController.archiveProducts(req.params).then(resultFromController => res.send(resultFromController));	
});*/
router.put("/:productId/archive", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProducts(data, req.params, req.body).then(resultFromController => res.send(resultFromController));
});


module.exports = router;
