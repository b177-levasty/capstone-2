const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	FirstName : {
		type : String,
		required : true
	},
	LastName : {
		type : String,
		required : true
	},
	MobileNum : {
		type : String,
		required : true
	},
	Orders :[
			{
				productId : {
					type : String,
					required : [true, `Product Id is require`]
			},
				purchasedOn : {
					type : Date,
					default : new Date
			},
				status : {
					type : Boolean,
					default : true
			}
	}]
})

module.exports = mongoose.model("Order", orderSchema);


			  
				