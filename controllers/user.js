const Product = require("../models/Product");
const User = require("../models/User");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Register User
// Register Order
module.exports.registerAccount = (reqBody) => {

	let newAccount = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNum : reqBody.mobileNum,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	let newCustomer = new Order ({
		FirstName : reqBody.firstName,
		LastName : reqBody.lastName,
		MobileNum : reqBody.mobileNum
	})

	let saveBoth

	return newAccount.save() && newCustomer.save().then((account, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})

/*	return newCustomer.save().then((customer, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})*/
}

// Get account by using the userId
module.exports.getAccount = (data) => {

	return User.findById(data.userId).then(result => {		
		result.password = "";

		return result;

	});

};

// log-in account with authenication
module.exports.loginAccount = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return false;
		}

		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				return { access : auth.createAccessToken(result)}
			}

			else{
				return false;
			}
		}
	})
}


// Set isAdmin to true by using the bearer token and the id of the user
module.exports.setAsAdmin = (data, reqParams) => {

	if(data.isAdmin) {
	let updateActiveField = {
		isAdmin : true
	};

	return User.findByIdAndUpdate(reqParams.userId, updateActiveField).then((course, error) => {

		if (error) {
			return false;

		} else {
			return true;

		}

	});
	}
	else {
  		return Promise.resolve(false);
	}
};

// For ordering
// dito HINDI PA
module.exports.order = async (data, isAdmin) => {

	
	if (isAdmin.isAdmin){
		return "false";
	}
	else {
		let isUserAccountUpdated = await User.findById(data.userId).then(user => {

			user.usersOrder.push({productId : data.productId}) 

			return user.save().then((user, error) => {
				if(error){
				return false;
				}
				else{
				return true;
				}
			})
		})
	
		
	let isProductUpdated = await Product.findById(data.productId).then(product => {
		
		product.customers.push({userId : data.userId})

		return product.save().then((product, error, req) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	let isOrderUpdated = await Order.findById(data.orderId).then(order => {
		
		order.Orders.push({productId : data.productId})

		return order.save().then((order, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

		if(isUserAccountUpdated && isProductUpdated && isOrderUpdated){
				return "true";
			}	
			else {
				return false;
			}
		}
	}	
	



// For get all orders
// Okay na
module.exports.getAllOrders = (data) => {

	if(data.isAdmin) {
		return Order.find({__v: {$gte: 1}}).then(result => {
			return result;

			})
	}
	else {
		return Promise.resolve(false);
	}
}
// For get users order

module.exports.getDetails = (data) => {

	if(data.isAdmin) {
		return false;
	
	}
	else {
		return User.findById(data.userId).then(result => {
			return result;
	});
  	
  }
};